const express = require('express')
const app = express()
const path  = require('path')

const PORT = 4000;

const _dirname = path.dirname("")
const buildPath = path.join(_dirname  , "../aakash-ui/build");

app.use(express.static(buildPath))

app.get("/*", function(req, res){

    res.sendFile(
        path.join(__dirname, "../aakash-ui/build/index.html"),
        function (err) {
          if (err) {
            res.status(500).send(err);
          }
        }
      );

})

app.listen(PORT, (error) =>{
  if(!error)
      console.log("Server is Successfully Running, and App is listening on port "+ PORT)
  else 
      console.log("Error occurred, server can't start", error);
  }
);